!#/usr/bin/env bash


#created by: ilayron
#purpose: practicing the lvm with pamcake
#date: 24/11/2020
#version: v1.0.0
#################################################3


main(){


mypart_for_ingridient

lvm_groups

rename_vgcake

extend_lvcake
}



# create partition for the ingridient

mypart_for_ingridient(){

fdisk /dev/sdd << EOF
n
p
2

+500M
t
8e
p
3

+500M
t
8e
w
EOF

mkdir /srv/{ingridients}

mount /dev/sdd2 /srv/ingridients

my_ingridient_array=["flour" "sugar" "eggs" "baking powder" "chocolate"] >> /srv/ingridients

 
}

lvm_groups(){
pvcreate /dev/sdd2 /dev/sdd3

vgcreate vgcake /dev/sdd2 /dev/sdd3

lvcreate -L200M -nlvcake vgcake


}

rename_vgcake(){

vgrename vgcake/vgpamcake 
 
}

extend_lvcake(){

fdisk /dev/sde << EOF
n
p
1


t
8e
w
EOF



vgextend vgpamcake /dev/sde1
pvextend /dev/sde1


}

main

